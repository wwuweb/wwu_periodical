<?php
/**
 * @file
 * wwu_periodical.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wwu_periodical_taxonomy_default_vocabularies() {
  return array(
    'publication_editions' => array(
      'name' => 'Publication Editions',
      'machine_name' => 'publication_editions',
      'description' => 'Periodical editions, such as "Winter" or "June"',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
