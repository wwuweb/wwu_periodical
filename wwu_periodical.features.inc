<?php
/**
 * @file
 * wwu_periodical.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_periodical_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_periodical_node_info() {
  $items = array(
    'periodical' => array(
      'name' => t('Publication'),
      'base' => 'node_content',
      'description' => t('A periodical media publication such as <em>Western Window</em>.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'periodical_class_note' => array(
      'name' => t('Publication Class Note'),
      'base' => 'node_content',
      'description' => t('<em>Class Notes</em> are a small snippet about a student, such as a graduation.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'periodical_issue' => array(
      'name' => t('Publication Issue'),
      'base' => 'node_content',
      'description' => t('A collection of stories for publication.'),
      'has_title' => '1',
      'title_label' => t('Issue Name'),
      'help' => '',
    ),
    'periodical_story' => array(
      'name' => t('Publication Story'),
      'base' => 'node_content',
      'description' => t('An article in a publication issue.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'periodical_story_author' => array(
      'name' => t('Publication Story Author'),
      'base' => 'node_content',
      'description' => t('The author of any publication story.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
